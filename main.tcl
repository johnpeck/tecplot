#!/opt/ActiveTcl-8.6/bin/tclsh
# Hey Emacs, use -*- Tcl -*- mode

set scriptname [file rootname $argv0]
set this_os $tcl_platform(os)

# ----------------------- Gnuplot settings ----------------------------

# The wxt terminal can keep windows alive after the gnuplot process
# exits.  This allows calling multiple persistent windows which allow
# zooming and annotation.
set gnuplot_terminal wxt

if {$this_os eq "Windows NT"} {
    set gnuplot "c:/Program Files/gnuplot/bin/wgnuplot.exe"    
} elseif {$this_os eq "Linux"} {
    set gnuplot gnuplot
}

# ---------------------- Command line parsing -------------------------
package require cmdline
set usage "usage: [file tail $argv0] \[options] filename"
set options {
}

try {
    array set params [::cmdline::getoptions argv $options $usage]
} trap {CMDLINE USAGE} {msg o} {
    # Trap the usage signal, print the message, and exit the application.
    # Note: Other errors are not caught and passed through to higher levels!
    puts $msg
    exit 1
}

########################## Data parameters ###########################

# The input data has a header row
set data_has_header true

dict set axes_dict plot title "Abbey + June noise measured by Logic Board"

dict set axes_dict ylabel title "Voltage spectral density"
dict set axes_dict ylabel units "DigU / rt(Hz)"

dict set axes_dict xlabel title "Frequency"
dict set axes_dict xlabel units "Hz"

###################### Done with configuration #######################


proc iterint {start points} {
    # Return a list of increasing integers starting with start with
    # length points
    set count 0
    set intlist [list]
    while {$count < $points} {
	lappend intlist [expr $start + $count]
	incr count
    }
    return $intlist
}

proc dashline {width} {
    # Return a string of dashes of length width
    set dashline ""
    foreach dashchar [iterint 0 $width] {
	append dashline "-"
    }
    return $dashline
}

# After cmdline is done, argv will point to the last argument
foreach filename $argv {
    lappend input_file_name_list $filename
}

proc iterint {start points} {
    # Return a list of increasing integers starting with start with
    # length points
    set count 0
    set intlist [list]
    while {$count < $points} {
	lappend intlist [expr $start + $count]
	incr count
    }
    return $intlist
}

proc lmean {input_list} {
    # Find the mean of a list
    set total 0
    set count 0
    foreach item $input_list {
	incr count
	set total [expr $total + $item]
    }
    set mean [expr $total/double($count)]
    return $mean
}

proc loffset {input_list offset} {
    # Move all list elements by offset
    foreach item $input_list {
	lappend no_offset_list [expr $item - $offset]
    }
    return $no_offset_list
}

proc lrms {input_list} {
    # Calculate RMS value of list
    set sum 0
    set count 0
    foreach item $input_list {
	incr count
	set sum [expr $sum + $item**2]
    }
    set rms [expr sqrt( $sum/double($count) )]
    return $rms
}

proc read_file {filename} {
    # Return a list of lines from an input file
    #
    # Arguments:
    #   filename -- relative or full path to file
    try {
	set fid [open $filename r]
    } trap {} {message optdict} {
	puts "Could not open file $filename"
	puts $message
	return
    }
    set datafile_list [split [read $fid] "\n"]
    return $datafile_list
}

proc write_gnuplot_data {filename voltage_V_list current_A_list temperature_C_list} {
    # Write a data file that can be plotted by gnuplot
    #
    # Arguments:
    #   filename -- Name of the output data file
    #   voltage_V_list -- List of drive voltages in volts
    #   current_A_list -- List of drive currents in amps
    #   temperature_C_list -- List of temperatures in Celsius
    global params
    if { [catch {open $filename w} fp] } {
	puts "Could not open $filename for writing"
	return
    }
    foreach voltage $voltage_V_list current $current_A_list temperature $temperature_C_list {
	puts $fp "$voltage $current $temperature"
    }
    close $fp
}

set plot_window_index 0

proc write_vi_script {filename args} {
    # Write a gnuplot script to plot current against voltage drive
    #
    # Arguments:
    #   filename -- Name of the gnuplot script to be written
    #   args -- Names of the files containing data to be plotted
    global gnuplot_terminal
    global scriptname
    global input_file_name
    global plot_window_index
    try {
	# Open file for appending in case there are multiple plots
	set fp [open $filename a]
    } trap {} {message optdict} {
	puts "Could not open $filename for writing"
	puts $message
	exit
    }
    set ylabel "TEC current"
    set yunits "A"
    set xlabel "TEC drive voltage"
    set xunits "V"
    set plot_title "Vigo PV-4TE-8 in enclosure on my desk"

    # Annotation x postion in graph (0 --> 1) coordinates
    set anno_x 0.05
    # Annotation y position in graph (0 --> 1) coordinates
    set anno_y 0.7
    # Spacing between annotation lines in graph (0 --> 1) coordinates
    set anno_incr 0.05
    puts $fp "reset"
    puts $fp "set terminal $gnuplot_terminal $plot_window_index size 800,600"
    puts $fp "set format y '%0.0s %c'"
    puts $fp "set format x '%0.1s %c'"
    puts $fp "set ylabel '$ylabel ($yunits)'"
    puts $fp "set xlabel '$xlabel ($xunits)'"
    puts $fp "set title '$plot_title'"
    puts $fp "set key center top"
    puts $fp "set yrange \[0:0.5\]"
    # Use noenhanced here to prevent underscores from becoming subscripts
    # set plot_instruction "using 1:2 with lines title '$input_file_name' noenhanced"
    set plot_instruction "using 1:2 with lines "
    set output_string "plot "
    foreach input_filename [split $args] {
	append output_string "'$input_filename' $plot_instruction "
	append output_string "title '[file tail $input_filename]' noenhanced, "
    }
    puts $fp [string trim $output_string ", "]



    puts $fp "set arrow from graph 0, first 0.37 to graph 1, first 0.37 nohead dt 2"
    puts $fp "set label 'Specified drive current' at graph 0.05, first 0.38 tc lt 1"
    puts $fp "set arrow from graph 0, first 0.4 to graph 1, first 0.4 nohead dt 2"
    puts $fp "set label 'Drive current limit' at graph 0.05, first 0.41 tc lt 1"
    puts $fp "set arrow from 5, graph 0 to 5, graph 1 nohead dt 2"
    puts $fp "set label 'Logic board limit' at 4.6, graph -0.08 tc lt 1"
    puts $fp "replot"

    puts $fp "set output 'tecplot_vi_plot.eps'"
    puts $fp "set terminal postscript eps color size 6in,4in"
    puts $fp "replot"
    puts $fp "set terminal wxt size 800,600"
    puts $fp "replot"
    incr plot_window_index
    close $fp
}

proc write_ti_script {filename args} {
    # Write a gnuplot script to plot temperature against voltage drive
    #
    # Arguments:
    #   filename -- Name of the gnuplot script to be written
    #   args -- Names of the files containing data to be plotted
    global gnuplot_terminal
    global scriptname
    global input_file_name
    global plot_window_index
    try {
	# Open file for appending in case there are multiple plots
	set fp [open $filename a]
    } trap {} {message optdict} {
	puts "Could not open $filename for writing"
	puts $message
	exit
    }
    set ylabel "Temperature from thermistor"
    set yunits "C"
    set xlabel "TEC drive voltage"
    set xunits "V"
    set plot_title "Vigo PV-4TE-8 in enclosure on my desk"

    # Annotation x postion in graph (0 --> 1) coordinates
    set anno_x 0.05
    # Annotation y position in graph (0 --> 1) coordinates
    set anno_y 0.7
    # Spacing between annotation lines in graph (0 --> 1) coordinates
    set anno_incr 0.05
    puts $fp "reset"
    puts $fp "set terminal $gnuplot_terminal $plot_window_index size 800,600"
    puts $fp "set format y '%0.0s %c'"
    puts $fp "set format x '%0.1s %c'"
    puts $fp "set yrange \[-80:30\]"
    puts $fp "set ylabel '$ylabel ($yunits)'"
    puts $fp "set xlabel '$xlabel ($xunits)'"
    puts $fp "set key center top"
    puts $fp "set title '$plot_title'"
    # Use noenhanced here to prevent underscores from becoming subscripts
    # set plot_instruction "using 1:2 with lines title '$input_file_name' noenhanced"
    set plot_instruction "using 1:3 with lines"
    set output_string "plot "
    foreach input_filename [split $args] {
	append output_string "'$input_filename' $plot_instruction "
	append output_string "title '[file tail $input_filename]' noenhanced, "
    }
    puts $fp [string trim $output_string ", "]
    puts $fp "set arrow from 5, graph 0 to 5, graph 1 nohead dt 2"
    puts $fp "set label 'Logic board limit' at 4.6, graph -0.08 tc lt 1"
    puts $fp "set arrow from graph 0, first -75 to graph 1, first -75 nohead dt 2"
    puts $fp "set label 'Specified temperature' at graph 0.05, first -73 tc lt 1"
    puts $fp "replot"
    # puts $fp "plot '$datafile' $plot_instruction"
    puts $fp "set output 'tecplot_ti_plot.eps'"
    puts $fp "set terminal postscript eps color size 6in,4in"
    puts $fp "replot"
    puts $fp "set terminal wxt size 800,600"
    puts $fp "replot"
    incr plot_window_index
    close $fp
}

########################## Main entry point ##########################

file delete gnuplot.gp

foreach input_file_name $input_file_name_list {
    lappend file_line_list [read_file $input_file_name]
    lappend input_file_base_list [file rootname $input_file_name]
}


foreach data_list $file_line_list {
    # Each data_list will be from a different input file
    set counter_list [iterint 0 [llength $data_list]]
    set voltage_V_list [list]
    set current_A_list [list]
    set temperature_C_list [list]
    foreach line $data_list counter $counter_list {
	if {$counter == 0 && $data_has_header} {
	    # This is the header row
	    continue
	}
	if {[llength $line] == 0} {
	    # This is a blank line
	    continue
	}
	set line_list [split $line ","]
	lappend voltage_V_list [lindex $line_list 0]
	lappend current_A_list [expr double([lindex $line_list 1])/1000]
	lappend temperature_C_list [expr [lindex $line_list 3] - 273]
    }
    lappend voltage_V_column_list $voltage_V_list
    lappend current_A_column_list $current_A_list
    lappend temperature_C_column_list $temperature_C_list
}


set measurement_dict [dict create]
# dict set measurement_dict bandwidth title "-3dB bandwidth"
# dict set measurement_dict bandwidth value [expr $3db_frequency_hz/1e6]
# dict set measurement_dict bandwidth units "MHz"
# 
# dict set measurement_dict shunt title "Shunt resistance"
# dict set measurement_dict shunt value 6.2
# dict set measurement_dict shunt units ohms

# Write data files that gnuplot can plot
set counter 0
foreach voltage_V_list $voltage_V_column_list \
    current_A_list $current_A_column_list \
    temperature_C_list $temperature_C_column_list \
    input_file_base $input_file_base_list {
	set gnuplot_data_filename "${input_file_base}.dat"
	lappend gnuplot_data_filename_list $gnuplot_data_filename
	write_gnuplot_data $gnuplot_data_filename $voltage_V_list \
	    $current_A_list $temperature_C_list
	incr counter
    }

# write_gnuplot_data gnuplot.dat [lindex $frequency_column_list 0] \
#     [lindex $sd_mag_column_list 0]
write_vi_script gnuplot.gp \
    {*}$gnuplot_data_filename_list


write_ti_script gnuplot.gp \
    {*}$gnuplot_data_filename_list


exec $gnuplot --persist gnuplot.gp
